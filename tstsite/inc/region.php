<?php
/**
 * Region filtering
 **/

 /* detect current region */
function tst_detect_current_region(){

    return empty($_COOKIE['raul_current_region_slug']) ? 'all-regions' : $_COOKIE['raul_current_region_slug'];
}


/* filter query */
add_action('parse_query', 'tst_filter_query_for_region');
function tst_filter_query_for_region($query){
	
	if(is_admin())
		return;
	
	if(!$query->is_main_query())
		return;
	
	if(!$query->is_archive() && !$query->is_search()) //@to_do test this carefully
		return;
	//var_dump($query->query_vars);
	$region = tst_detect_current_region();
	if($region == 'all-regions') // no need for filter
		return;
	
	$query->query_vars['region'] = 'all-regions,'.$region;
}

/* dropdown */
function tst_region_dropdown(){
	
	$terms = get_terms('region', array(
		'hide_empty' => false,
		'parent' => 0		
	));
	
	if(empty($terms))
		return;
	
	$options = array();
	//$options[] = array('name' => 'Выберите регион', 'value' => 'no_filter');
		
	foreach($terms as $t_obj){
		$label = ($t_obj->slug == 'all-regions') ? 'Выберите регион' : esc_attr($t_obj->name);
		$options[] = array('name' => $label, 'value' => $t_obj->slug);
	}
	
?>
	<form id="site-region" method="post" action="">
	<label for="region_filter"><select name="region_filter" id="region_filter">
	<?php
		foreach($options as $opt){
			echo "<option value='".$opt['value']."' ".(isset($_COOKIE['raul_current_region_slug']) && $_COOKIE['raul_current_region_slug'] == $opt['value'] ? 'selected' : '').">".$opt['name']."</option>";
		}
	?>
	</select></label>
	</form>
<?php
}
