<?php
/**
 * Site specifictemplate tags
 **/

/**
 * Modifications
 **/

/* config **/
function tst_cpt_prime_taxes() {
	$taxes = array(
		'post'      => 'category',
		'event'     => 'eventcat',
		'vacancy'   => 'vacat',
		'method'    => 'metcat',
		'companies' => 'comcat'
	);
	
	return $taxes;
}
 
/* posts formats */
add_action('init', 'frl_post_formats', 100);
function frl_post_formats(){
	remove_post_type_support('post', 'post-formats');     	
	deregister_taxonomy_for_object_type('post_format', 'post');
}
 
/* CPT Filters */
add_filter('request', 'frl_request_corrected', 10);
function frl_request_corrected($query_vars) {
	
	
	if(isset($query_vars['post_type']) && $query_vars['post_type'] == 'companies'){
		$query_vars['posts_per_page'] =  -1; //@to_do: make it real
		$query_vars['orderby'] = 'menu_order';
	}
	
	if(isset($query_vars['post_type']) && $query_vars['post_type'] == 'vacancy'){
		
		if(!isset($query_vars['vacat'])){
			$query_vars['orderby'] = 'meta_value date';
			$query_vars['meta_key'] = 'vacancy_status';
			$query_vars['order'] = 'DESC';
			
			//var_dump($query_vars);
		}
		
	}
	
	//if(isset($query_vars['tag']) && !empty($query_vars['tag']))
	//	$query_vars['post_type'] =  array('post', 'work', 'article');
		
	return $query_vars;
} 


/* sorting field for vacanccies */
add_action('save_post', 'tst_hidden_vacancy_field', 2, 2);
function tst_hidden_vacancy_field($post_id, $post){
	
	if($post->post_type != 'vacancy')
		return;
	
	$terms = wp_get_object_terms($post_id, 'vacat');
	$value = (isset($terms[0]) && $terms[0]->slug == 'open') ? 'open' : 'warchive';
	update_post_meta($post_id, 'vacancy_status', $value);
}


/* Admin */
add_filter('manage_posts_columns', 'frl_common_columns_names', 50, 2);
function frl_common_columns_names($columns, $post_type) {
		
	if($post_type == 'companies' || $post_type == 'banner'){
		$columns['menu_order'] = 'Порядок';
	}
	
	if(in_array($post_type, array('post', 'event', 'method', 'news', 'companies', 'vacancy', 'banner', 'attachment'))){
		
		$columns['taxonomy-audience'] = 'Аудитория';
		$columns['taxonomy-region'] = 'Регион';
		
		$columns['id'] = 'ID';
		
		if($post_type != 'attachment' && $post_type != 'vacancy')
			$columns['thumbnail'] = 'Миниат.';
					
	}
	
	return $columns;
}

add_filter('manage_pages_columns', 'frl_pages_columns_names');
function frl_pages_columns_names($columns) {
	$columns['taxonomy-audience'] = 'Аудитория';
	
	return $columns;
}

add_action('manage_posts_custom_column', 'frl_common_columns_content', 2, 2);
function frl_common_columns_content($column_name, $post_id) {
	
	$cpost = get_post($post_id);
	
	if($column_name == 'id'){
		echo intval(get_post($post_id)->ID);
		
	} elseif($column_name == 'thumbnail') {
		$img = get_the_post_thumbnail($post_id, 'thumbnail');
		if(empty($img))
			echo "&ndash;";
		else
			echo "<div class='admin-tmb'>{$img}</div>";
			
		//format
		if(in_array($cpost->post_type, array('news', 'post'))){
			$format = tst_get_media_format($post_id); 
			$format_label = $format; //@to_do get label here
			echo "<div class='format-label'><small>{$format_label}</small></div>";
		}	
	}
	elseif($column_name == 'menu_order') {
		echo intval($cpost->menu_order);
	}
}

/* admin tax columns */
add_filter('manage_taxonomies_for_event_columns', function($taxonomies){
	//$taxonomies[] = 'pr_type';
	$taxonomies[] = 'eventcat';
	
    return $taxonomies;
});

add_filter('manage_taxonomies_for_method_columns', function($taxonomies){
	
	$taxonomies[] = 'metcat';
	
    return $taxonomies;
});

add_filter('manage_taxonomies_for_companies_columns', function($taxonomies){
	
	$taxonomies[] = 'comcat';
	
    return $taxonomies;
});

add_filter('manage_taxonomies_for_vacancy_columns', function($taxonomies){
	
	$taxonomies[] = 'vacat';
	
    return $taxonomies;
});


 
/* Custom conditions */
function is_about(){
	global $post;
	
	if(!is_page())
		return false;
	
	if(is_page('about'))
		return true;
	
	$parents = get_post_ancestors($post);
	$test = get_page_by_path('about');
	if(in_array($test->ID, $parents))
		return true;
	
	return false;
}

function is_page_branch($slug){
	global $post;
	
	if(empty($slug))
		return false;
	
		
	if(!is_page())
		return false;
	
	if(is_page($slug))
		return true;
	
	$parents = get_post_ancestors($post);
	$test = get_page_by_path($slug);
	if(in_array($test->ID, $parents))
		return true;
	
	return false;
}

function is_materials() {
	
	if(is_post_type_archive('article'))
		return true;
	
	if(is_tax('art_cat'))
		return true;
	
	if(is_singular('article'))
		return true;
	
	return false;
}

function is_tax_branch($slug, $tax) {
	global $post;
	
	$test = get_term_by('slug', $slug, $tax);
	if(empty($test))
		return false;
	
	if(is_tax($tax)){
		$qobj = get_queried_object();
		if($qobj->term_id == $test->term_id || $qobj->parent == $test->term_id)
			return true;
	}
	
	if(is_singular() && is_object_in_term($post->ID, $tax, $test->term_id))
		return true;
	
	return false;
}

function is_news() {
	
	if(is_home() || is_category())
		return true;
	
	if(is_singular('post'))
		return true;
	
	return false;
}

function is_events() {
	
	if(is_post_type_archive('event'))
		return true;
	
	if(is_tax('eventcat'))
		return true;
	
	if(is_singular('event'))
		return true;
	
	return false;
}





/** Audience related functions **/
function tst_current_audience(){	
	global $post;
	
	$audience = get_query_var('audience');
	if(is_page()) {
		$a_term = wp_get_object_terms($post->ID, 'audience');
		if(!empty($a_term))
			$audience = $a_term[0]->slug;
	}
	
	if(empty($audience))
		$audience = 'no_filter';
	
	
	return $audience;
}

function tst_audience_menu() {
	$audience = tst_current_audience();
	
	wp_nav_menu(array(
		'theme_location'  => 'audience-'.$audience,
		//'menu'          => , 
		'container'       => 'div',
		'container_class' => 'sidenav-holder',
		'menu_class'      => 'audience-'.$audience,
		'menu_id'         => 'audience-menu',
		'echo'            => true,                
		'depth'           => 0, 
	));
}

function tst_inpage_listing_nav(){
	
	$all_link = '';
	
	$post_type = get_query_var('post_type');
	if(empty($post_type))
		$post_type = 'post';
		
	$taxes = tst_cpt_prime_taxes();
	
	if(!isset($taxes[$post_type]))
		return;
	
	$terms = get_terms($taxes[$post_type], array('parent' => 0));
	if(empty($terms))
		return;
		
	
	if($post_type != 'post'){
		$all_link = get_post_type_archive_link($post_type);		
	}
	else {
		$all_link = home_url('stories');
	}
		
	$audience = tst_current_audience();
	if($audience != 'no_filter')
		$all_link = add_query_arg('audience', $audience, $all_link);
	
	//prepare all link as base
	$items = array();
	$items[] = "<li><a href='{$all_link}'>Все</a></li>";
	
	$has_current = false;
	foreach($terms as $term){
		$link = add_query_arg($term->taxonomy, $term->slug, $all_link);
		$css = '';
		if(isset($_GET[$term->taxonomy]) && $_GET[$term->taxonomy] == $term->slug) {
			$css = ' class="current-menu-item"';
			$has_current = true;
		}
		
		$items[] = "<li{$css}><a href='{$link}'>".$term->name."</a></li>";		
	}
	
	if(!$has_current){
		$items[0] = str_replace("<li>", '<li class="current-menu-item">', $items[0]);
	}
?>
	<div class="inpage-nav-wrap cf"><ul class="inpage-menu cf">
	<?php  echo implode('', $items);?>
	</ul></div>
<?php
}

function tst_inpage_listing_title($post_type = null) {
	
	if(empty($post_type))
		$post_type = get_query_var('post_type');
	
	if(empty($post_type)) {
		$story_page_id = get_option('page_for_posts');
		$story_page = get_page($story_page_id);
		if(!empty($story_page))
			return apply_filters('post_type_archive_title', $story_page->post_title);
	}
	else {
		
		$post_type_obj = get_post_type_object($post_type);		
		return apply_filters('post_type_archive_title', $post_type_obj->labels->name);
	}
}


/* posts elements */
function tst_image_format_thumbnail($size = 'embed'){
	
	$thumb_id = get_post_thumbnail_id(get_the_ID());
	$url = wp_get_attachment_url($thumb_id);
	$img = wp_get_attachment_image($thumb_id, $size);
	
	echo "<a href='{$url}' rel='image-overlay' class='img-padder fresco'>{$img}</a>";
}


function tst_partner_meta() {
	
	if(!function_exists('get_field'))
		return;
	
	$website = get_field('company_web', get_the_ID());
	$desc = get_field('company_activity', get_the_ID());
	
?>
	<ul class="company-data">
	<?php if(!empty($desc)):?>
		<li class="company-activity"><?php echo $desc;?></li>
	<?php endif; if(!empty($website)):?>
		<li class="company-web"><?php echo make_clickable($website);?></li>
	<?php endif;?>
	</ul>
<?php
}

function tst_partner_portfolio() {
	
	if(!function_exists('get_field'))
		return;
	
	$portfolio = get_field('company_portfolio', get_the_ID());
	if($portfolio) {
		echo "<h3 class=' widget-title'>Галерея</h3>";
		echo apply_filters('frl_the_content', $portfolio);
	}
}

function tst_partner_logo(){
	
	$img = get_the_post_thumbnail(get_the_ID(), 'full');
	if(!empty($img)):
?>
	<div class="logo-frame"><span><?php echo $img;?></span></div>
<?php
	endif;
}


function tst_vacancy_metafield($key){
	
	if(!function_exists('get_field_object'))
		return; 
	
	$field = get_field_object($key);
	
	if(!empty($field['value'])):
?>
	<div class="vc-meta-field <?php echo $key;?>">
		<h5 class='vc-meta-title'><?php echo $field['label'];?></h5>
		<div class="vc-meta-value"><?php echo apply_filters('frl_the_content', $field['value']);?></div>
	</div>
	
<?php endif;
	
}

function tst_vacancy_company() {
	
	if(!function_exists('get_field'))
		return;
	
	$org = get_field('vacan_contract');
	$addr = get_field('vacan_location');
	
	if(!empty($org)):
?>
	
	<div class="org"><?php echo $org;?></div>
	<div class="addr"><em>адрес:</em> <?php echo $addr;?></div>
	
<?php
	endif;
}



function tst_vacancy_undertitle_meta() {
	
	if(!function_exists('get_field'))
		return;
	
	$org = get_field('vacan_contract');
	$wage = get_field('vacan_wages');
	
	if(!empty($org)){
		echo "<div class='org'>{$org}</div>";
	}
		
?>
	<div class='submeta'>
	<?php if(is_object_in_term(get_the_ID(), 'vacat', 'open')):?>
		<?php if(!empty($wage)):?>
			<span class='wage'><em>Оклад:</em> <?php echo $wage;?></span>
		<?php endif;?>
	<?php else:?>
		<span class='wage'><em>Специалист найден.</em></span>
	<?php endif;?>
		<span class="pubdate"><?php printf('<em>Опубликовано:</em> %s', "<time>".get_the_date()."</time>"); ?></span>		
	</div>
<?php
}

function is_vacancy_open($vac_id = null) {
	global $post;
	
	if(!$vac_id && $post)
		$vac_id = $post->ID;
		
	if($vac_id && is_object_in_term($vac_id, 'vacat', 'open'))
		return true;
	
	return false;
}





/**
 * Shortcodes
 **/
add_shortcode('q', 'tst_q_screen'); 
function tst_q_screen($atts, $content = null) {
	
	if(empty($content))
		return '';
	
	$out = "<div class='tst-question'>";
	$out .= apply_filters('frl_the_content', $content);
	$out .= "</div>";
	
	return $out;
}


/**
 * Team list
 * list of people with descriptions
 **/
function la_banner_format_team_list_ext($query, $format_args) {
	global $post;
	
	//prepare args	
	$format_args = explode(',', $format_args);
	$format_args = array_map('trim', $format_args);
	$size = (isset($format_args[0]) && !empty($format_args[0])) ? $format_args[0] : 'post-thumbnail'; 
	$css = (isset($format_args[1]) && !empty($format_args[1])) ? $format_args[1] : 'regular';
	
	//check for plugin
	if(!class_exists('La_Banner_Core'))
		return '';		
	
	$out = '<div class="team-list '.$css.'">';
	while($query->have_posts()): $query->the_post();
	
	$out .= "<article class='banner-full frame'>";
	
	$contact =  apply_filters('frl_the_content', $post->post_excerpt);
	$logo = get_the_post_thumbnail($post->ID, $size);
	$title = apply_filters('the_title', $post->post_title);
	$desc = apply_filters('frl_the_content', $post->post_content);
	
	$out .= "<div class='bit-3'><div class='person-logo'>{$logo}</div></div>";
	$out .= "<div class='bit-5'><h4>{$title}</h4><div class='details'>{$desc}</div></div>";
	$out .= "<div class='bit-4'><div class='contacts'>{$contact}</div></div>";
	$out .= "</article>";
		
	endwhile; wp_reset_postdata();
    $out .= "</div>";


    return $out;
}

/** counters **/
function la_banner_format_counters($query, $format_args) {
	global $post;
	
	//check for plugin
	if(!class_exists('La_Banner_Core'))
		return '';
	
	$out = '<ul class="counters-list">';
	while($query->have_posts()):
        $query->the_post();
        $out .= "<li class='counter'>".($post->post_excerpt ? '<a href="'.$post->post_excerpt.'">' : '').apply_filters('frl_the_content', $post->post_content).($post->post_excerpt ? '</a>' : '')."</li>";
	endwhile;
    wp_reset_postdata();
    $out .= "</ul>";
	
	return $out;
}

add_shortcode('vacancy_counter', 'vacancy_counter_screen');
function vacancy_counter_screen($atts){
	
	$c = wp_count_posts('vacancy');
	return intval($c->publish);
}

add_shortcode('intro_text', 'intro_text_screen');
function intro_text_screen($atts, $content = null){
	
	if(empty($content))
		return '';
	
	$out = "<div class='intro-text'>".apply_filters('frl_the_content', $content)."</div>";
	return $out;
}

?>