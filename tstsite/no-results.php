<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 */
?>

<section class="no-results not-found">
	
	<h2><?php _e('Nothing found', 'tst');?></h2>

	<div class="section-content">

		<p><?php _e('Unfortunately, no materials were found under your request', 'tst');?></p>
	</div><!-- .page-content -->
</section><!-- .no-results -->
