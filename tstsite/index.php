<?php
/**
 * The main template file.
 **/


$audience = tst_current_audience(); 
$pt = get_query_var('post_type');

get_header(); ?>


<div class="frame">
	
	<div class="bit-2 listing-nav audience-<?php echo esc_attr($audience);?>">
		<?php tst_audience_menu() ?>
	</div>
	
	<div class="bit-10 listing">
		<header class="page-header">
			<h1 class="page-title"><?php echo tst_inpage_listing_title();?></h1>
		</header>
		<nav class="page-nav">
			<!-- inpage menu -->
			<?php tst_inpage_listing_nav();?>
		</nav>		
		<div class="page-body in-loop">
			<?php if ( have_posts() ) : ?>
			
				<?php if($pt == 'companies') echo "<div class='frame'>"; ?>
			
					<?php while ( have_posts() ) : the_post(); ?>				
						<?php get_template_part('content', get_post_type()); ?>
					<?php endwhile; ?>
				
				<?php if($pt == 'companies') echo "</div>"; ?>
		
				<?php tst_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>
		</div>
	</div>
	
</div><!-- .frame -->


<?php get_footer(); ?>