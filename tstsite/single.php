<?php
/**
 * Single template
 **/



get_header(); 

while ( have_posts() ) : the_post();

?>	
<article id="post-<?php the_ID(); ?>" <?php post_class('single-item'); ?>>

<h1 class="single-title entry-title"><?php the_title(); ?></h1>

<div class="frame">
	<?php
		$template = 'single';

		$pt = get_post_type();
		if(in_array($pt, array('vacancy', 'event'))){
			$template = 'single_'.$pt;
		}

		get_template_part('content', $template);
	?>
</div><!-- .frame -->

<?php get_sidebar();?>

</article>
<?php endwhile; ?>

<?php get_footer(); ?>