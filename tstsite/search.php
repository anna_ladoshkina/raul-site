<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package Blank
 */

$audience = '';
 
get_header(); ?>

<h1 class="page-title"><?php  _e('Search results', 'tst');?></h1>

<div class="frame">
	
	<div class="bit-8 listing">
		
			
		<?php if(function_exists('la_rs_search_results_form')) la_rs_search_results_form($wp_query);?>
		
		
		<div class="page-body in-loop">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ) : the_post(); ?>				
					<?php
					if(function_exists('la_rs_loop_entry'))
						la_rs_loop_entry();
					else
						get_template_part('content');
				?>	
				<?php endwhile; ?>
		
				<?php tst_content_nav( 'nav-below' ); ?>

			<?php else : ?>

				<?php get_template_part( 'no-results', 'index' ); ?>

			<?php endif; ?>
		</div>
	</div>
	
	<div class="bit-4">
		<?php dynamic_sidebar('search-sidebar');?>
	</div>	
</div><!-- .frame -->

	
<?php get_footer(); ?>