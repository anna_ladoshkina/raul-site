<?php
/**
 * The Template for displaying single page
 * 
 */

$audience = tst_current_audience();

get_header(); ?>


<div class="frame">
	
	<div class="bit-2 listing-nav audience-<?php echo esc_attr($audience);?>">
		<?php tst_audience_menu() ?>
	</div>
	
	<div class="bit-10 listing">
		<?php if(have_posts()) : while ( have_posts() ) : the_post(); ?>
		<header class="page-header">
			<h1 class="page-title"><?php echo the_title();?></h1>
		</header>
		<nav class="page-nav">
			<!-- inpage menu -->
			
		</nav>		
		<div class="page-body in-page">
			<?php the_content(); ?>
		</div>
		<?php endwhile; endif; ?>
	</div>
	
</div><!-- .frame -->

<?php get_footer(); ?>