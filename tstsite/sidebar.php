<?php
/**
 * The Sidebar 
 */

$pt = get_post_type(); ?>

<aside class="entry-related">
    <?php
    if($pt == 'companies')
        tst_partner_portfolio();
    else if($pt == 'vacancy')
        tst_related_posts_gallery(null, 'post_tag', 4, array('meta' => false, 'not_found_msg' => false, 'list_title' => 'Еще вакансии'));
    else
        tst_related_posts_gallery(null, 'post_tag', 4, array('meta' => false, 'not_found_msg' => false));

    echo '&nbsp;';?>

</aside>