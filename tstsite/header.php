<?php
/**
 * The Header for our theme.
 */
?><!doctype html>
<!--[if lt IE 7 ]> <html <?php language_attributes(); ?> class="no-js ie6" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 7 ]>    <html <?php language_attributes(); ?> class="no-js ie7" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 8 ]>    <html <?php language_attributes(); ?> class="no-js ie8" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 9 ]>    <html <?php language_attributes(); ?> class="no-js ie9" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta charset="<?php bloginfo('charset'); ?>">    
	<meta name="viewport" content="width=device-width" >
	<link rel="profile" href="http://gmpg.org/xfn/11">
		
	<title><?php wp_title( '|', true, 'right' ); ?></title>

	<?php wp_head(); ?>
	<!--Microsoft -->
    <meta http-equiv="cleartype" content="on">
		
</head>
<?php flush(); ?>

<body id="top" <?php body_class(); ?>>
<div id="page" class="hfeed site">
	
	<header id="masthead" class="site-header" role="banner"><div class="inner">
	
		<div class="site-utility cf">
			<!-- regional filter -->
			<?php tst_region_dropdown();?>
			<?php get_search_form();?>
		</div>
			
		<div class="site-branding">
			<div class="frame">
		
			<div class="bit-8">
				
				<h1 class="site-title ir">
				<?php if(!is_front_page()):?>	
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				<?php else:?>
					<?php bloginfo( 'name' ); ?>
				<?php endif;?>
				</h1>				
			</div>
			
			<div class="bit-4">
				<!-- descriptive text -->
				<?php dynamic_sidebar('header-sidebar'); ?>
			</div>
			
			</div><!-- .frame -->
		
		</div>
		
	</div></header>
	
	

	<div id="content" class="site-content">