<?php
/**
 * @package Blank
 */


?>
<article id="post-<?php the_ID(); ?>" <?php post_class('loop-item'); ?>>

<div class="frame">
	<div class="bit-1"><div class="dashicons dashicons-book"></div></div>
	
	<div class="bit-11">
		<header class="entry-header">
			<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				
			<div class="entry-meta"><?php tst_posted_on(); ?></div>
				
		</header><!-- .entry-header -->		
			
		<div class="entry-summary"><?php the_excerpt(); ?></div>			
	</div>

</div>
	
</article><!-- #post-## -->
