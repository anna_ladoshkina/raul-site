<?php
/**
 * @package Blank
 */

$pt = get_post_type();
$format = tst_get_media_format();
$split_content = frl_single_split_content($post);

?>
<div class="bit-4">
	<?php if($pt == 'companies'):?>
		<div class="entry-preview logo-wrap"><?php tst_partner_logo();?></div>
		
	<?php else: ?>
		<?php if($format == 'none'):?>
			<div class="entry-preview"><?php the_post_thumbnail();?></div>
		<?php endif;?>		
			<div class="entry-summary"><?php echo apply_filters('the_excerpt', $split_content['intro']); ?></div>		
	
	<?php endif; ?>
	
	<div class="entry-meta">
		
		<?php if($pt == 'companies'):?>
			<?php tst_partner_meta();?>
			
		<?php else: ?>
			<?php tst_posted_on(); ?>
			
		<?php endif;?>
	</div>
	
	<div class="entry-utility">
		<?php frl_page_actions();?>
		<?php get_template_part('return', 'block'); ?>
	</div>

</div>

<div class="bit-8">
<?php if($format != 'none'): ?>
	<div class="entry-media <?php echo esc_attr($format);?> cf">	
		<?php echo tst_get_media_html(array('image_size' => 'embed'));?>
	</div>
	
	<div class="entry-content">
		<?php echo apply_filters('the_content', $split_content['content']); ?>
	</div>
<?php else:?>
	<div class="entry-content"><?php the_content();?></div>
<?php endif;?>

</div>