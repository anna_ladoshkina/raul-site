<?php
/**
 * @package Blank
 */

$css = (is_object_in_term(get_the_ID(), 'vacat', 'open')) ? 'open' : 'archived';
$mark = (is_object_in_term(get_the_ID(), 'vacat', 'open')) ? 'Открыта' : 'Специалист найден';
?>
<div class="bit-4 <?php echo $css;?>">
	
    <div class="vc-status">
        <div class="dashicons dashicons-category"></div>
        <div class="status"><?php echo $mark;?></div>        
        <div class="pubdate"><?php printf('Опубликовано: %s', "<time>".get_the_date()."</time>"); ?></div>
    </div>
	
    <div class="vc-metas">
        <?php
            tst_vacancy_metafield('vacan_wages');
            tst_vacancy_metafield('vacan_schedule');
            tst_vacancy_metafield('vacan_registration');
        ?>
    </div>
    
    
    <div class="vc-actions"><?php dynamic_sidebar('vacancy-sidebar');?></div>
    
    
	<div class="entry-utility">
		<?php frl_page_actions();?>
		<?php get_template_part('return', 'block');?>		
	</div>
	
</div>

<div class="bit-8">
    
    
    
	<div class="entry-content">
        <div class="vc-company"><?php tst_vacancy_company();?></div>
        <div class="vc-summary"><?php the_content();?></div>
        <div class="vc-data">
        <?php 
            $data = array(
                'vacan_status',
                'vacan_function',
                'vacan_skills',
                'vacan_availability',
                'vacan_maintenance',
                'vacan_maintenance_fund',                
            );
            
            foreach($data as $key) {
                tst_vacancy_metafield($key);
            }
        ?>
        </div>
    </div>
    
</div><!-- .bit-8 -->