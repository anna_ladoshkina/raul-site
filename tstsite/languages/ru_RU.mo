��            )         �     �  
   �  7   �               !     2     A     T  
   b  
   m     x  ,   �     �     �     �     �  -   �           .     =  C   M     �     �     �  9   �     �     �  6   	     @  g  N     �     �  R   �  '   1     Y  "   j  *   �  (   �  .   �     	     (	     B	  V   `	  =   �	      �	  	   
  $    
  v   E
      �
  !   �
  %   �
  �   %     �            ^   *  %   �  2   �  �   �     �                                                                                                                    	   
                 &laquo; prev. All events All materials of the site are avaliabe under license %s Choose the Taxonomy:  Contacts Display Excerpt? Display Metas? Display Thumbnail? Event address Event date Event time Excerpt Length: Let's try to find an information you needed. No related posts found. Nothing found Num. Page is not found Related items selected by some taxonomy terms Related posts Search results Select taxonomy Specify thumbnail size. Leave blank to apply size defined by theme. Thumbnail size Title Top Unfortunately, no materials were found under your request Upcoming events Upcoming events list We're sorry, but there is no such page on our website. next. &raquo; Project-Id-Version: Blank v1.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-04-12 20:46+0400
PO-Revision-Date: 2014-04-12 20:47+0400
Last-Translator: Anna <nordworldofann@gmail.com>
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ../
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: .
 &laquo; пред. Все события Все материалы сайта доступны под лицензией %s Выберите таксономию:  Контакты Отображать цитату? Отображать метаданные? Отображать миниатюру? Место проведения события Дата события Время события Длина аннотации Давайте попробуем найти нужную вам информацию. К сожалению, материалы не найдены Ничего не найдено Числ. Страница не найдена Материалы по теме, выбранные по совпадению некоторых таксономий Материалы по теме Результаты поиска Выберите таксономию Укажите размер миниатюры. Пустое значение приведёт к использованию размера по умолчанию для данной темы. Размер миниатюры Заголовок К началу К сожалению, по вашему запросу материалы не найдены Предстоящие события Список предстоящих событий К сожалению, запрошенная страница не найдена на нашем сайте. Возможно, она была перемещена. след. &raquo; 