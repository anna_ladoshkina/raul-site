<?php
/**
 * Template Name: Homepage
 * The Template for homepage
 * 
 */

function tst_home_excerpt_length($l){
	
	return 30;
}

$region = tst_detect_current_region();

//vacancy
$vargs = array(
	'post_type' => 'vacancy',
	'posts_per_page' => 4,
	'tax_query' => array(		
		array(
			'taxonomy' => 'vacat',
			'field' => 'slug',
			'terms' => 'open'
		)
	)
);

if($region != 'all-regions') {
	$vargs['tax_query'][] = array(
		'taxonomy'=> 'region',
		'field' => 'slug',
		'terms' => array('all-regions', $region)
	);
	$vargs['tax_query']['relation'] = 'AND';
}

//var_dump($vargs);
$v_query = new WP_Query($vargs);


if($v_query->found_posts < 4) {
	$vargs = array(
		'post_type' => 'vacancy',
		'posts_per_page' => 4,		
	);
	
	if($region != 'all-regions') {
		$vargs['tax_query'][] = array(
			'taxonomy'=> 'region',
			'field' => 'slug',
			'terms' => array('all-regions', $region)
		);		
	}	
	$v_query = new WP_Query($vargs);
}


//events
$eargs = array(
	'post_type' => 'event',
	'posts_per_page' => 1,
	'tax_query' => array(
		array(
			'taxonomy' => 'eventcat',
			'field' => 'slug',
			'terms' => 'anonsy'
		)
	),
	'orderby' => 'meta_value',
	'meta_key' => 'event_date_expire',
	'order' => 'ASC'
);

if($region != 'all-regions') {
	$eargs['tax_query'][] = array(
		'taxonomy'=> 'region',
		'field' => 'slug',
		'terms' => array('all-regions', $region)
	);
	$eargs['tax_query']['relation'] = 'AND';
}	

$e_query = new WP_Query($eargs);

if($e_query->found_posts < 1) {
	$eargs = array(
		'post_type' => 'event',
		'posts_per_page' => 1,
		'orderby' => 'meta_value',
		'meta_key' => 'event_date_expire',
		'order' => 'DESC'
	);
	if($region != 'all-regions') {
		$eargs['tax_query'][] = array(
			'taxonomy'=> 'region',
			'field' => 'slug',
			'terms' => array('all-regions', $region)
		);
	}	

	$e_query = new WP_Query($eargs);
}


//news
$nargs = array(
	'post_type' => 'news',
	'posts_per_page' => 4,	
);

if($region != 'all-regions') {
	$nargs['tax_query'][] = array(
		'taxonomy'=> 'region',
		'field' => 'slug',
		'terms' => array('all-regions', $region)
	);	
}
$n_query = new WP_Query($nargs);


//partners
$pargs = array(
	'post_type' => 'companies',
	'posts_per_page' => 150,
	'orderby' => 'rand'
);
if($region != 'all-regions') {
	$pargs['tax_query'][] = array(
		'taxonomy'=> 'region',
		'field' => 'slug',
		'terms' => array('all-regions', $region)
	);	
}
$p_query = new WP_Query($pargs);


//button
$homepage = get_option('page_on_front');
$call_link = $call_label = '';

if(!empty($homepage) && function_exists('get_field')){
	$call_link = get_field('call_link', $homepage);
	$call_label = get_field('call_label', $homepage);	
}

get_header(); ?>
	
<section class="home-section home-slider"><div class="inner">

	<?php frl_cycloneslider('homeslider');?>
	
	<div class="counter-wrap">
		<?php echo do_shortcode("[la_banner format='counters' num='-1' type='counters' orderby='menu_order']");?>
	</div>
</div></section>

<section class="home-section home-callout"><div class="inner">

	<div class="frame">
	<div class="bit-9"> <?php the_content();?></div>
	<div class="bit-3">
		<div class="action-call"><a href="<?php echo esc_url($call_link);?>">
			<div class="call-icon"><div class="dashicons dashicons-awards"></div></div>
			<div class="call-label"><?php echo $call_label;?></div>
		</a></div>
	</div>
	</div>

</div></section>

<section class="home-section home-widgets"><div class="inner">
	
	<div class="frame">
		
		<div class="bit-3 home-vacancy home-loop">
			<h3 class="widget-title"><a href="<?php echo site_url('/vacancies/?audience=business');?>">Вакансии</a></h3>
			<?php if($v_query->have_posts()):?>			
			<ul class="home-list">
				<?php  while($v_query->have_posts()): $v_query->the_post();?>
				<li class="hl-item">
					<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
					<div class="hi-meta"><?php if(function_exists('get_field')) { echo get_field('vacan_contract');}?></div>
				</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; wp_reset_postdata(); ?>
		</div>
		
		<div class="bit-3 home-event">
			<h3 class="widget-title"><a href="<?php echo site_url('/events/?audience=all');?>">События</a></h3>
			<?php if($e_query->have_posts()): while($e_query->have_posts()): $e_query->the_post(); ?>			
			<div class="h-item">
				<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
				<div class="hi-meta"><?php echo tst_event_date(); ?></div>
				<?php add_filter( 'excerpt_length', 'tst_home_excerpt_length', 50); ?>
				<div class='hi-summary'><?php the_excerpt();?></div>
				<?php remove_filter( 'excerpt_length', 'tst_home_excerpt_length', 50); ?>
			</div>
			<?php endwhile; endif;  wp_reset_postdata();?>
		</div>
		
		<div class="bit-3 home-news home-loop">
			<h3 class="widget-title"><a href="<?php echo site_url('/news/?audience=all');?>">Новости</a></h3>
			<?php if($n_query->have_posts()):?>			
			<ul class="home-list">
				<?php  while($n_query->have_posts()): $n_query->the_post();?>
				<li class="hl-item">
					<h4><a href="<?php the_permalink();?>"><?php the_title();?></a></h4>
					<div class="hi-meta"><?php tst_posted_on();?></div>
				</li>
				<?php endwhile; ?>
			</ul>
			<?php endif; wp_reset_postdata(); ?>
		</div>
		
		<div class="bit-3 home-partners">
			<h3 class="widget-title"><a href="<?php echo site_url('/partners/?audience=all');?>">Партнеры</a></h3>
			
			<div class="h-slider">
				<a class="h-prev" href="#"><span class="arrow"></span></a>
				<a class="h-next" href="#"><span class="arrow"></span></a>
				<div class="h-slideshow">
				<?php
					if($p_query->have_posts()):
					
					$counter = 0; 					
					while($p_query->have_posts()): $p_query->the_post();
					
						if($counter > 1)
							$counter = 0;
						
						if($counter == 0)
							echo '<div class="h-slide">';							
							//print logo
					?>
						<div class="hi-preview logo-wrap">
							<div class="logo-frame">
								<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('full');?></a>
							</div>
						</div>
					<?php	
						if($counter == 1)
							echo '</div>';
							
						$counter++;
					endwhile; 
					
					endif; wp_reset_postdata(); ?>
					
				</div>				
			</div>
		</div>
		
	</div><!-- .frame -->
	
</div></section>



<?php get_footer(); ?>