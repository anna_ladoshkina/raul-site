<?php
/**
 * @package Blank
 */


?>
<div class="bit-4">
	
	<div class="entry-preview"><?php the_post_thumbnail();?></div>
		
	
	<div class="entry-meta">
		
		<?php tst_event_undertitle_meta();?>
		<?php tst_event_contacts_block();?>			
		
	</div>
	
	<div class="entry-utility">
		<?php frl_page_actions();?>
		<?php get_template_part('return', 'block');?>		
	</div>
	
</div>

<div class="bit-8">

	<div class="entry-content">
		<?php the_content(); ?>		
		<?php tst_event_map_block();?>
		
	</div>

</div>