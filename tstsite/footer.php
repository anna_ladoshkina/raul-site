<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Blank
 */

$cc_link = '<a href="http://creativecommons.org/licenses/by-sa/3.0/">Creative Commons СС-BY-SA 3.0</a>';

?>

	</div><!-- .inner #content -->
	
	<div id="bottombar" class="widget-area"><div class="inner">
		
		<div class="frame">
			<div class="bit-4"><?php dynamic_sidebar( 'footer_one-sidebar' );?></div>
			<div class="bit-4"><?php dynamic_sidebar( 'footer_two-sidebar' );?></div>
			<div class="bit-4"><?php dynamic_sidebar( 'footer_three-sidebar' );?></div>
		</div>
	
	</div></div>
	
	<footer id="colophon" class="site-footer" role="contentinfo"><div class="inner">
		
		<div class="frame">
		
			<div class="bit-4">
				<div class="copy"><a href="<?php home_url();?>"><?php bloginfo('name');?></a>. <?php printf(__('All materials of the site are avaliabe under license %s', 'tst'), $cc_link);?></div>
			</div>
			
			<div class="bit-4"><?php dynamic_sidebar( 'footer-sidebar' );?></div>
			<div class="bit-4">
				<?php 
					wp_nav_menu(array(
						'theme_location'  => 'social',
						//'menu'          => , 
						'container'       => 'div',
						'container_class' => 'social-holder',
						'menu_class'      => 'cf',
						'menu_id'         => 'social-menu',
						'echo'            => true,                
						'depth'           => 0, 
					));
				?>
			</div>
		</div><!-- .frame -->
		
		
	</div></footer>
	
</div><!-- #page -->

<!-- menu panels -->
<nav id="bottom-navigation" class="bottom-panel"><div class="inner site">
<div class="close">Закрыть</div>
<?php 
	wp_nav_menu(array(
		'theme_location'  => 'primary',
		//'menu'          => , 
		'container'       => false,				
		'menu_class'      => 'pmenu frame',
		'menu_id'         => 'global-menu',
		'echo'            => true,                
		'depth'           => 0, 
	));
?>
</div></nav><!-- #site-navigation -->

<nav id="side-navigation" class="side-panel"><div class="inner">
<div id="side-panel-trigger"><span class="open">&larr;</span><span class="close">&rarr;</span></div>
<?php 
	wp_nav_menu(array(
		'theme_location'  => 'audience-all',
		//'menu'          => , 
		'container'       => false,				
		'menu_class'      => 'pmenu',
		'menu_id'         => 'about-menu',
		'echo'            => true,                
		'depth'           => 0, 
	));
?>
</div></nav><!-- #site-navigation -->

<?php wp_footer(); ?>

</body>
</html>