<?php
/**
 * @package Blank
 */

$css = (is_object_in_term(get_the_ID(), 'vacat', 'open')) ? 'loop-item open' : 'loop-item'; 
?>
<article id="post-<?php the_ID(); ?>" <?php post_class($css); ?>>

<div class="frame">
	<div class="bit-1"><div class="dashicons dashicons-category"></div></div>
	
	<div class="bit-11">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
		<div class="vc-metas"><?php tst_vacancy_undertitle_meta();?></div>	
		<div class="vc-summary"><?php the_excerpt();?></div>		
	</div>

</div>
	
</article><!-- #post-## -->
