<?php
/**
 * Return block on single pages
 **/

global $post;

$post_type = get_post_type();
$audience = wp_get_object_terms(get_the_ID(), 'audience');
if(empty($audience) || !function_exists('get_field'))
	return;

$li = array();
foreach($audience as $aterm){
	
	$support = get_field('audience_post_types_for_return', 'audience_'.$aterm->term_id);
	$string = array();
	if($support == 'all' || false !== strpos($support, $post_type)){
		//audience label
		$css = esc_attr($aterm->slug);
		$string[] = "<span class='label'>".apply_filters('single_term_title', $aterm->name)."</span>";
		
		//audience link
		$link = get_post_type_archive_link($post_type);
		$link = add_query_arg($aterm->taxonomy, $aterm->slug, $link);
		$txt = tst_inpage_listing_title($post_type);
		$string[] = "<a href='{$link}'>{$txt}</a>";
		
		//category link
		$categories = tst_cpt_prime_taxes();
		if(isset($categories[$post_type])){
			$tax = $categories[$post_type];
			$terms = wp_get_object_terms(get_the_ID(), $tax);
			if(!empty($terms)){
				$link_c = add_query_arg($tax, $terms[0]->slug, $link);
				$txt_c = apply_filters('single_term_title', $terms[0]->name);
				$string[] = "<a href='{$link_c}'>{$txt_c}</a>";
			}
		}
		
	}
	$li[] = "<li class='$css'>".implode('//', $string)."</li>";
}
?>

<nav class="in-single">
<div id="backblock" class="return-block">
	<div class="return-trigger"><span class="label">Вернуться</span> <span class="arrow"></span></div>
	<div class="return-content">
		<ul class="back-menu"><?php echo implode('', $li);?></ul>
	</div>
</div>
</nav>