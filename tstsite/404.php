<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Blank
 */


get_header(); ?>


<header class="page-header">
	<h1 class="page-title"><?php echo __('Page is not found', 'tst');?></h1>
</header>

	
<div class="page-body in-page">

	<section class="not-found-404">
		
	<div class="frame">
		<div class="bit-4">
			<div class="err-mark">404</div>
		</div>
		
		<div class="bit-8">
			<p><?php _e('We\'re sorry, but there is no such page on our website.', 'tst');?></p>
			<p><?php _e('Let\'s try to find an information you needed.', 'tst');?></p>
			
			<?php get_search_form(); ?>
		</div>
		
	</section><!-- .error-404 -->

</div>

<?php get_footer(); ?>