<?php
/**
 * Blank functions and definitions
 *
 * @package Blank
 */

/**
 * Initials
 **/
if(!isset($content_width))
	$content_width = 640; /* pixels */

if(!isset($tst_main_w)){ //setting of main content wrappers
	
	$tst_nav_w = 0;
	$tst_main_w = 8;
	$tst_side_w = 4;
}


if ( ! function_exists( 'tst_setup' ) ) :
function tst_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Blank, use a find and replace
	 * to change 'blank' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'tst', get_template_directory() . '/languages' );

	
	//add_theme_support( 'automatic-feed-links' );

	/**
	 * Images
	 **/
	add_theme_support( 'post-thumbnails' );
	
	/* image sizes */
	set_post_thumbnail_size(390, 244, true ); // regular thumbnails
	add_image_size('logo', 220, 140, true ); // logo thumbnail 
	//add_image_size('poster', 220, 295, true ); // poster in widget	
	add_image_size('embed', 640, 400, true ); // fixed size for embending
	add_image_size('long', 640, 280, true ); // long thumbnail for pages

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary'   => 'Главное меню',
		'audience-all' => 'Боковое меню - О проекте',
		'audience-business' => 'Боковое меню - Бизнесу',
		'audience-people' => 'Боковое меню - Наставникам',
		'audience-ngo' => 'Боковое меню - НКО',			
		'social'    => 'Социальные кнопки'
	));

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array('image', 'video', 'gallery'));

	
}
endif; // blank_setup
add_action( 'after_setup_theme', 'tst_setup' );


/**
 * Register widgetized area and update sidebar with default widgets
 */
function tst_widgets_init() {
	
	$config = array(
		'vacancy' => array(
						'name' => 'Вакансии - действия',
						'description' => 'Динамическая область слева на странице вакансии'
					),
		'search' => array(
						'name' => 'Поиск',
						'description' => 'Динамическая область справа на странице результатов поиска'
					),
		'header' => array(
						'name' => 'Шапка',
						'description' => 'Динамическая область в шапке сайта'
					),				
		'footer' => array(
						'name' => 'Футер',
						'description' => 'Динамическая нижняя область'
					),
		
		/*'home_one' => array(
						'name' => 'Главная - 1/3',
						'description' => 'Динамическая нижняя область - 1 колонка'
					),
		'home_two' => array(
						'name' => 'Главная - 2/3',
						'description' => 'Динамическая нижняя область - 2 колонка'
					),
		'home_three' => array(
						'name' => 'Главная - 3/3',
						'description' => 'Динамическая нижняя область - 3 колонка'
					)	*/
	);
		
	
	foreach($config as $id => $sb) {
		
		$before = '<div id="%1$s" class="widget %2$s">';
		
		if(false !== strpos($id, 'footer')){
			$before = '<div id="%1$s" class="widget %2$s bottom">';
		}
		elseif(false !== strpos($id, 'header')) {
			$before = '<div id="%1$s" class="header-block">';
		}
		
		register_sidebar(array(
			'name' => $sb['name'],
			'id' => $id.'-sidebar',
			'description' => $sb['description'],
			'before_widget' => $before,
			'after_widget' => '</div>',
			'before_title' => '<h3 class="widget-title">',
			'after_title' => '</h3>',
		));
	}
}
add_action( 'widgets_init', 'tst_widgets_init' );


function tst_custom_title( $title, $sep ) {

//    if(is_feed())
//        return $title;

    if(is_page('homepage'))
        $title = get_bloginfo('name');

    else if(is_page()) {

        $audience = reset(wp_get_post_terms(get_the_ID(), 'audience'));
        $title = get_the_title()." $sep ".$audience->name." $sep ".get_bloginfo('name');

    } else if(is_single())
        $title = get_the_title()." $sep ".get_bloginfo('name');

    else if(is_post_type_archive('news')) {

        $audience = get_term_by('slug', $_GET['audience'], 'audience');
        $title = "Новости $sep ".$audience->name." $sep ".get_bloginfo('name');

    } else if(is_post_type_archive('event')) {

        $audience = get_term_by('slug', $_GET['audience'], 'audience');
        if( !empty($_GET['eventcat']) ) {

            $category = get_term_by('slug', $_GET['eventcat'], 'eventcat');
            $title = "События $sep ".$category->name." $sep ".$audience->name." $sep ".get_bloginfo('name');
        } else
            $title = "События $sep ".$audience->name." $sep ".get_bloginfo('name');

    } else if(is_post_type_archive('vacancy')) {

        $audience = get_term_by('slug', $_GET['audience'], 'audience');
        if( !empty($_GET['vacat']) ) {

            $category = get_term_by('slug', $_GET['vacat'], 'vacat');
            $title = "Вакансии $sep ".$category->name." $sep ".$audience->name." $sep ".get_bloginfo('name');
        } else
            $title = "Вакансии $sep ".$audience->name." $sep ".get_bloginfo('name');
    } else if(is_post_type_archive('method')) {

        $audience = get_term_by('slug', $_GET['audience'], 'audience');
        if( !empty($_GET['metcat']) ) {

            $category = get_term_by('slug', $_GET['metcat'], 'metcat');
            $title = "Методики $sep ".$category->name." $sep ".$audience->name." $sep ".get_bloginfo('name');
        } else
            $title = "Методики $sep ".$audience->name." $sep ".get_bloginfo('name');
    } else if(is_post_type_archive('companies')) {

        $audience = get_term_by('slug', $_GET['audience'], 'audience');
        if( !empty($_GET['comcat']) ) {

            $category = get_term_by('slug', $_GET['comcat'], 'comcat');
            $title = "Партнёры $sep ".$category->name." $sep ".$audience->name." $sep ".get_bloginfo('name');
        } else
            $title = "Партнёры $sep ".$audience->name." $sep ".get_bloginfo('name');
    } else if(is_post_type_archive('post')) {

        $audience = get_term_by('slug', $_GET['audience'], 'audience');
        if( !empty($_GET['category']) ) {

            $category = get_term_by('slug', $_GET['category'], 'category');
            $title = "Истории $sep ".$category->name." $sep ".$audience->name." $sep ".get_bloginfo('name');
        } else
            $title = "Истории $sep ".$audience->name." $sep ".get_bloginfo('name');
    }

    return $title;
}
add_filter('wp_title', 'tst_custom_title', 100, 2);


/**
 * Enqueue scripts and styles
 */
function tst_scripts() {

	$url = get_template_directory_uri();

	wp_enqueue_style('gfonts', 'http://fonts.googleapis.com/css?family=Tenor+Sans&subset=latin,cyrillic', array());		
	wp_enqueue_style('design', $url.'/css/design.css', array('dashicons'));

	wp_enqueue_script('front', $url.'/js/front.js', array('jquery', 'jquery-cycle2'), '1.0', true);
	wp_enqueue_script('cookie', $url.'/js/jquery.cookie.js', array('jquery', 'front'), '1.0', true);

}
add_action( 'wp_enqueue_scripts', 'tst_scripts', 100 );

function tst_admin_scripts() {
	
	$url = get_template_directory_uri();	
	
	wp_enqueue_style('tst-admin', $url.'/css/admin.css', array());
	
}
add_action( 'admin_enqueue_scripts', 'tst_admin_scripts' );

/**
 * Custom additions.
 */
require get_template_directory().'/inc/template-tags.php';
require get_template_directory().'/inc/extras.php';
require get_template_directory().'/inc/related.php';
require get_template_directory().'/inc/events.php';
require get_template_directory().'/inc/region.php';

