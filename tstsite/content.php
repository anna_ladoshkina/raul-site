<?php
/**
 * @package Blank
 */

$column_class = ' bit-8';
$thumb_class = ' bit-4';
$pt = get_post_type();
?>

<?php if($pt == 'companies'): ?>
<article id="post-<?php the_ID(); ?>" <?php post_class('loop-item bit-3'); ?>>
	
	<div class="entry-preview logo-wrap">
		<div class="logo-frame"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail('full');?></a></div>
	</div>
	
	<h1 class="partner-title">
		<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?>
		<span><?php if(function_exists('get_field')) echo get_field('company_activity', get_the_ID());?></span>
		</a>
	</h1>
	
</article><!-- #post-## -->

<?php else: ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('loop-item'); ?>>
	
	
	<div class="frame">
			
			<div class="entry-preview<?php echo $thumb_class;?>"><?php the_post_thumbnail();?></div>
			
		
		<div class="entry-column<?php echo $column_class;?>">
			
			<header class="entry-header">
				<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
				
				<div class="entry-meta">
					<?php tst_posted_on(); ?>
				</div>
				
			</header><!-- .entry-header -->		
			
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div>		
			
		</div>
		
	</div>
	
		
</article><!-- #post-## -->
<?php endif;?>