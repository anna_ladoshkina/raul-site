/* scripts */

/* Top link Plugin by David Walsh (http://davidwalsh.name/jquery-top-link)
  rewritten by @foralien to be safely used in no-conflict mode */

(function($) {
 
	$.fn.topLink = function(settings) {
	    var config = {
	    	'min'       : 400,
	    	'fadeSpeed' : 200
	    };
 
		if (settings) $.extend(config, settings);
 
		this.each(function() {
       		//listen for scroll
			var el = $(this);
			el.hide(); //in case the user forgot
			
			$(window).scroll(function() {
				if($(window).scrollTop() >= settings.min){
					el.fadeIn(settings.fadeSpeed);
					
				} else {
					el.fadeOut(settings.fadeSpeed);
				}
			});			
    	});
 
    	return this; 
	};
 
})(jQuery);


jQuery(function($){
		
	$('html').removeClass('no-js').addClass('js');
	
	/* no rich interations fot ie6/ie7 */
	var windowWidth = $(window).width();
	var testIE = false;
	if($('html').hasClass('ie6') || $('html').hasClass('ie7')){
		testIE = true;
	}
	
	/* top link	 */
	var toplinkTrigger = $('#top-link');
	
	if( windowWidth > 600 && !testIE ) {
		toplinkTrigger
		.topLink({ //appearance
			min: 400,
			fadeSpeed: 500
			
		})
		.on('click', function(event){ //smoth scroll
			event.preventDefault();
			var full_url = toplinkTrigger.find('a').attr('href');
			
			var parts = full_url.split("#");
			var trgt = parts[1];
			
			var target_offset = $("#"+trgt).offset();
			var target_top = target_offset.top;
			
				
			$('html, body').animate({scrollTop:target_top}, 900);
		});    
	}
	
	/* about panel */
	$('#side-panel-trigger, #about-menu > li > a').on('click', function(e){

        e.preventDefault();
		
		var container = $('#side-navigation');
		if (container.hasClass('wide')) {
			var modify = 70;
			if (windowWidth < 1280) {
				modify += 85;
			}
			container.animate({width: "-="+ modify}, 300, function(){container.removeClass('wide');});
		}
		else {
			container.animate({width: "+="+ modify}, 300, function(){container.addClass('wide');});			
		}
	});
	
	/* bottom panel */
	$('#bottom-navigation').on('click', '.menu-item-has-children > a, .close', function(e){
		
		e.preventDefault();
		var container = $('#bottom-navigation');
		if (container.hasClass('wide')) {
			container.animate({bottom: "-=300"}, 400, function(){container.removeClass('wide');});
		}
		else {
			container.animate({bottom: "+=300"}, 400, function(){container.addClass('wide');});			
		}
	});
	
	
	//partners slider	
	$('.cycle-slideshow').cycle({
		speed: 600,
		manualSpeed: 100
	});
	
	/* return block */
	$('#backblock').on('click','.return-trigger', function(e){
		e.preventDefault();
		var container = $('#backblock');
		
		if (container.hasClass('toggled')) {			
			container.find('.return-content').slideUp(function(){ container.removeClass('toggled')});
		}
		else {
			container.find('.return-content').slideDown(function(){ container.addClass('toggled')});
		}
	});
	
	//partners slider
	$('.h-slideshow').cycle({
		speed: 600,
        slides: '.h-slide',		
        next: '.h-next',
        prev: '.h-prev',
        pauseOnHover: true,
        fx: 'scrollHorz',
		paused: false,		
        carouselVisible: 1,
		delay: 100,
		timeout: 2000
	});
	
	$('#region_filter').change(function(){

        $.cookie('raul_current_region_slug', $(this).val(), {expires: 100, path: '/'}); // Cookie will live for 100 days
        window.location.href = window.location.href;
    });
});

